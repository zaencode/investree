<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weather extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
   {
		 parent::__construct();
		 $this->load->helper('url');
		 $this->load->library('xmlrpc');
		 $this->load->library('xmlrpcs');
   }

	public function index()
	{
		$forcastdata['data_found'] = false;
		$this->load->view('weather',$forcastdata);
	}

	public function search()
  {
		if(isset($_POST['location'])){
			$city = $this->input->post('location');
		}else{
			$city = "Jakarta";
		}

    $searchurl = "https://www.metaweather.com/api/location/search/?query=".$city;
		$searchdata = json_decode($this->curlURL($searchurl));
		$forcastdata = new stdClass();
		if(count($searchdata) === 0){
			$forcastdata->data_found = false;
		}else{
			if(strtolower($city) === strtolower($searchdata[0]->title)){
				$woeid = $searchdata[0]->woeid;
				$forcasturl = "https://www.metaweather.com/api/location/".$woeid."/";
				$forcastdata = json_decode($this->curlURL($forcasturl));
				$forcastdata->data_found = true;
			}else{
				$forcastdata->data_found = false;
			}
		}
		$this->load->view('weather',$forcastdata);
  }

	private function curlURL($url){
		$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $data = curl_exec($ch);

    if (curl_errno($ch)){
      print "Error: " . curl_error($ch);
    }else{
      $transaction = json_decode($data, TRUE);
      curl_close($ch);
      return $data;
    }
	}
}
