<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

		<title>Investree Weather</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?=base_url('assets/fonts/font-awesome.min.css')?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?=base_url('assets/style.css')?>">

		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>


	<body>

		<div class="site-content">

			<div class="hero" data-bg-image="<?=base_url('assets/images/banner.png')?>">
				<div class="container">
					<form action="<?=base_url('weather/search/')?>" method = "POST" class="find-location">
						<input type="text" name = "location" placeholder="Find your location...">
						<input type="submit" value="Find">
					</form>

				</div>
			</div>
			<div class="forecast-table">
				<div class="container">
					<div class="forecast-container">
						<?php if($data_found){?>
						<div class="today forecast">
							<div class="forecast-header">
								<div class="day"><?=date('l', strtotime($consolidated_weather[0]->applicable_date))?></div>
								<div class="date"><?=date('M j', strtotime($consolidated_weather[0]->applicable_date))?></div>
							</div> <!-- .forecast-header -->
							<div class="forecast-content">
								<div class="location"><?=$title?></div>
								<div class="degree">
									<div class="num"><?=round($consolidated_weather[0]->the_temp)?><sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="https://www.metaweather.com/static/img/weather/<?=$consolidated_weather[0]->weather_state_abbr?>.svg" alt="" width=90>
									</div>
								</div>
								<span><img src="<?=base_url('assets/images/icon-umberella.png')?>" alt=""><?=round($consolidated_weather[0]->humidity)?>%</span>
								<span><img src="<?=base_url('assets/images/icon-wind.png')?>" alt=""><?=round($consolidated_weather[0]->wind_speed)?>km/h</span>
								<span><img src="<?=base_url('assets/images/icon-compass.png')?>" alt=""><?php 	$dirs = array('North', 'North East', 'East', 'South East', 'South', 'South West', 'West', 'North West', 'North'); echo $dirs[round($consolidated_weather[0]->wind_direction/45)]; ?></span>
							</div>
						</div>
						<?php foreach ($consolidated_weather as $key => $value) { if ($key < 1) continue;?>
						<div class="forecast">
							<div class="forecast-header">
								<div class="day"><?=date('l', strtotime($consolidated_weather[$key]->applicable_date))?></div>
							</div> <!-- .forecast-header -->
							<div class="forecast-content">
								<div class="forecast-icon">
									<img src="https://www.metaweather.com/static/img/weather/<?=$consolidated_weather[$key]->weather_state_abbr?>.svg" alt="" width=48>
								</div>
								<div class="degree"><?=round($consolidated_weather[$key]->the_temp)?><sup>o</sup>C</div>
								<small>Min : <?=round($consolidated_weather[0]->min_temp)?><sup>o</sup>C</small><br>
								<small>Max : <?=round($consolidated_weather[0]->max_temp)?><sup>o</sup>C</small>
							</div>
						</div>
					<?php } ?>
				<?php }else{ echo "<H1>Data Not Found</H1>";} ?>
					</div>
				</div>
			</div>

			<footer class="site-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<p class="colophon">Copyright 2018 Fakhrizal Dzuhri. All rights reserved</p>
						</div>
						<div class="col-md-3 col-md-offset-1">
							<div class="social-links">
								<a href="https://www.facebook.com/princefafa"><i class="fa fa-facebook"></i></a>
								<a href="https://twitter.com/princefafa"><i class="fa fa-twitter"></i></a>
								<a href="https://plus.google.com/u/0/111527715836367629878"><i class="fa fa-google-plus"></i></a>
							</div>
						</div>
					</div>
				</div>
			</footer> <!-- .site-footer -->
		</div>

		<script src="<?=base_url('assets/js/jquery-1.11.1.min.js')?>"></script>
		<script src="<?=base_url('assets/js/plugins.js')?>"></script>
		<script src="<?=base_url('assets/js/app.js')?>"></script>

	</body>

</html>
